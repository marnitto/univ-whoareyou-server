# -*- coding: utf-8 -*-


import requests
import json

SERVER_KEY = 'AIzaSyCrg0fLGq3OCEFxF4fJxc_flSKG4IjUMWs'
google_gcm_server = 'https://gcm-http.googleapis.com/gcm/send'

def send(req_id, title=u'Alert', message=u'Alert!!'):
    print 'SEND called'
    data = {
        'data': {'title': title, 'message': message},
        'to': req_id
    }
    headers = {
        'Authorization': 'key='+SERVER_KEY,
        'Content-Type': 'application/json'
    }

    # TODO: delegate to celery worker
    return requests.post(google_gcm_server,
        data=json.dumps(data),
        headers=headers).text
