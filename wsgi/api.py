# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import datetime
import functools
import json
import os

from flask import Blueprint
import flask
import sqlalchemy
from sqlalchemy.orm.exc import NoResultFound
from wand.image import Image

import gcm
import model
from model import session


api = Blueprint('api', __name__)


@api.route('/base/login', methods=['POST'])
def api_base_login():
    sess = session()
    userid = flask.request.form['id']
    userpw = flask.request.form['pw']

    try:
        u = sess.query(model.User).filter_by(name=userid).one()
    except NoResultFound:
        return flask.jsonify(result=False)

    if not u.check_password(userpw):
        return flask.jsonify(result=False)

    flask.session['user'] = u.name
    return flask.jsonify(result=True)


def login_required(f):
    @functools.wraps(f)
    def login_check(*arg, **kwarg):
        if 'user' in flask.session:
            return f(*arg, **kwarg)

        # No session, try to POST login
        if flask.request.method == 'POST' and \
            'id' in flask.request.form and \
            'pw' in flask.request.form:
                res = api_base_login().data.decode('utf-8')
                if json.loads(res)['result'] is True:
                    return f(*arg, **kwarg)

        flask.abort(403)

    return login_check


def recent_incident(user):
    '''Get recent(~30 min) model.Incident. If not, create new'''

    sess = session()
    since = datetime.datetime.utcnow() - datetime.timedelta(minutes=30)

    inc = sess.query(model.Incident) \
        .filter(model.Incident.end_time >= since) \
        .filter_by(user_id=user.id) \
        .order_by(sqlalchemy.desc(model.Incident.end_time)) \
        .first()
    if inc is None:
        inc = model.Incident()
        inc.count = sess.query(model.Incident) \
            .filter_by(user_id=user.id).count() + 1
        user.incidences.append(inc)
        with sess.begin():
            sess.add(inc)

    return inc


def get_login_user():
    sess = session()
    try:
        u = sess.query(model.User).filter_by(name=flask.session['user']).one()
    except NoResultFound:
        flask.session.pop('user', None)
        return None

    return u


@api.route('/base/log', methods=['POST'])
@login_required
def api_base_log():
    sess = session()
    message = flask.request.form['message']
    u = get_login_user()
    if not u:
        return flask.jsonify(result='error',
            reason='Invalid user id. Please re-login.')

    inc = recent_incident(u)
    inc.logs.append(model.Log(message=message))
    inc.end_time = datetime.datetime.utcnow()
    sess.merge(inc)

    return flask.jsonify(result='success')


@api.route('/base/image', methods=['POST'])
@login_required
def api_base_image_upload():
    sess = session()
    image = flask.request.files['image']
    u = get_login_user()
    if not u:
        return flask.jsonify(result='error',
            reason='Invalid user id. Please re-login.')

    # Register to database
    inc = recent_incident(u)
    img = model.Image()
    img.set_hash(image)

    # Before commiting, Check if same image has been already stored
    # We don't need to waste storage
    uri = ['img', u.name, img.filename]
    path = os.path.join(os.getcwd(), 'wsgi', 'static', os.path.join(*uri))

    if os.path.exists(path):
        return flask.jsonify(result='ignore', \
            path=flask.url_for('static', filename='/'.join(uri)))

    # New image, add image list
    inc.images.append(img)
    inc.end_time = datetime.datetime.utcnow()
    sess.merge(inc)

    # Save files in webroot/static
    try:
        os.makedirs(os.path.split(path)[0])
    except OSError:
        pass

    with Image(file=image) as img:
        img.rotate(180)
        img.save(filename=path)

    return flask.jsonify(result='success', path='/static/'+'/'.join(uri))


@api.route('/base/image/<int:incident_count>')
@login_required
def api_base_image_list(incident_count):
    sess = session()
    u = get_login_user()
    if not u:
        return flask.jsonify(result='error',
            reason='Invalid user id. Please re-login.')

    res = ['/static/img/'+u.name+'/'+i.filename \
            for i in u.incidences[incident_count-1].images]
    return flask.jsonify(result=res)


@api.route('/base/status', methods=['GET', 'POST'])
@login_required
def api_base_status_get():
    sess = session()
    u = get_login_user()
    if not u:
        return flask.jsonify(result='error',
            reason='Invalid user id. Please re-login.')

    return flask.jsonify(
        result='success', intruding=u.intruding, monitoring=u.monitoring)


@api.route('/base/status/<cmd>/<switch>', methods=['GET', 'POST'])
@login_required
def api_base_status_set(cmd, switch):
    sess = session()
    u = get_login_user()
    if not u:
        return flask.jsonify(result='error',
            reason='Invalid user id. Please re-login.')

    if switch not in ('on', 'off') or cmd not in ('intruding', 'monitoring'):
        return flask.jsonify(result='error',
            reason='Invalid argument.')

    prev_switch = u.intruding
    setattr(u, cmd, (switch == 'on'))

    # Turn off intruding switch if monitoring is being off
    if cmd == 'monitoring' and switch == 'off':
        u.intruding = False

    with sess.begin():
        sess.add(u)

    # If intruding is being on, send notification to user's all mobile device
    # using GCM API key.
    #
    # FIXME: User - UserDevice (back)ref not working on query
    #        temporary using dumb session.query()
    devices = sess.query(model.UserDevice).filter_by(user_id=u.id).all()
    gcm_count = len(devices)

    if cmd == 'intruding' and switch == 'on' and prev_switch is False:
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        message = '가택 침입자가 발생했습니다! 앱에서 정보를 확인하세요.'
        message += ' (%s)' % now

        for dev in devices:
            gcm.send(
                req_id=dev.notify_key,
                title='침입 탐지 경고!',
                message=message)

    return flask.jsonify(result='success', notify_count=gcm_count)


@api.route('/mobile/auth', methods=['POST'])
def api_mobile_auth():
    '''Handles mobile device authentication for message notification service.

    Args:
        request_id - GCM client API key.

    Returns:
        result - User ID found in server database, otherwise empty string.
    '''

    ret = dict(result='')
    request_id = flask.request.form['request_id']

    sess = session()
    try:
        u = sess.query(model.User) \
            .filter(model.User.id == model.UserDevice.user_id) \
            .filter(model.UserDevice.notify_key == request_id) \
            .one()

        ret['result'] = u.name
    except NoResultFound:
        pass

    return flask.jsonify(**ret)


@api.route('/mobile/register', methods=['POST'])
def api_mobile_register():
    '''Register GCM client API key to given account.

    If given ID does not exist in server, create a new account and link user ID
    with request_id so that user can receive GCM notification message
    immediately. If server knows given ID(already registered), try to login and
    link.

    Args:
        request_id - GCM client API key to register.
        id - Account ID.
        pw - Account PW.

    Returns:
        result - "success" if successfully registerered
                 "fail" if login failed
                 "ignore" if duplicated
        reason (optional) - reason why fails
    '''

    request_id = flask.request.form['request_id']
    username = flask.request.form['id']
    userpw = flask.request.form['pw']

    sess = session()
    try:
        u = sess.query(model.User).filter_by(name=username).one()
        if not u.check_password(userpw):
            return flask.jsonify(result='fail', reason='Invalid password.')

        # Login success
        try:
            ud = sess.query(model.UserDevice) \
                .filter_by(notify_key=request_id).one()

            # and device already authorized
            return flask.jsonify(result='ignore')
        except NoResultFound:
            # Register UserDevice (- User)
            ud  = model.UserDevice(user_id=u.id, notify_key=request_id)
            with sess.begin():
                sess.add(ud)

            return flask.jsonify(result='success')

    except NoResultFound:
        # User not exist, create new
        u = model.User(name=username)
        u.set_password(userpw)
        u.devices = [model.UserDevice(user_id=u.id, notify_key=request_id)]
        with sess.begin():
            sess.add(u)

        return flask.jsonify(result='success')

    return flask.jsonify(result='fail', reason='Reached EOF')
