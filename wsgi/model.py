# -*- coding: utf-8 -*-

import datetime
import hashlib

import sqlalchemy
from sqlalchemy import (
    Boolean, Column, DateTime, Float, ForeignKey, Integer, Table, String,
    Unicode)
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.orm.session import sessionmaker


Base = declarative_base()
engine = sqlalchemy.create_engine('mysql://who:are@mariadb/you')
session = scoped_session(sessionmaker(engine, autocommit=True))


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(Unicode(128), unique=True)
    passhash = Column(String(40), nullable=False)
    intruding = Column(Boolean, default=False)
    monitoring = Column(Boolean, default=True)

    def check_password(self, password):
        return hashlib.sha1(
            self.name.encode('utf-8')+password.encode('utf-8')) \
            .hexdigest() == self.passhash

    def set_password(self, password):
        self.passhash = hashlib.sha1(
            self.name.encode('utf-8')+password.encode('utf-8')) \
            .hexdigest()


class Incident(Base):
    __tablename__ = 'incidences'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    count = Column(Integer)
    start_time = Column(DateTime, default=datetime.datetime.utcnow)
    end_time = Column(DateTime, default=datetime.datetime.utcnow)

    user = relationship('User', backref=backref('incidences'))


class UserDevice(Base):
    __tablename__ = 'user_devices'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    notify_key = Column(String(200))  # GCM key

    user = relationship('User', backref=backref('devices', lazy='noload'))


class Log(Base):
    __tablename__ = 'logs'

    id = Column(Integer, primary_key=True)
    incident_id = Column(Integer, ForeignKey('incidences.id'))
    timestamp = Column(DateTime, default=datetime.datetime.utcnow)
    message = Column(String(1024))

    incident = relationship('Incident', backref=backref('logs'))


class Image(Base):
    __tablename__ = 'images'

    id = Column(Integer, primary_key=True)
    incident_id = Column(Integer, ForeignKey('incidences.id'))
    timestamp = Column(DateTime, default=datetime.datetime.utcnow)
    filename = Column(String(1024))
    hash_md5 = Column(String(32), nullable=False)
    hash_sha1 = Column(String(40), nullable=False)

    def set_hash(self, fileobj):
        content = fileobj.read()
        self.hash_md5 = hashlib.md5(content).hexdigest()
        self.hash_sha1 = hashlib.sha1(content).hexdigest()
        self.filename = hashlib.md5('%s_%s' % (len(content), self.hash_md5)) \
            .hexdigest()+'.jpg'

        fileobj.seek(0)

    incident = relationship('Incident', backref=backref('images'))
