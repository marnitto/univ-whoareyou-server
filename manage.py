# -*- coding: utf-8 -*-

from __future__ import absolute_import

import codecs
import os

import click
import sadisplay
import sqlalchemy

from wsgi import wsgi, model


@click.group()
def manage():
    pass


@manage.command()
def install():
    engine = sqlalchemy.create_engine('mysql://who:are@mariadb/you')
    model.Base.metadata.create_all(engine)


@manage.command()
def dbimage():
    desc = sadisplay.describe([getattr(model, attr) for attr in dir(model)])
    with codecs.open('/tmp/schema.dot', 'wb', 'utf-8') as f:
        f.write(sadisplay.dot(desc))

    os.system('dot -Tpng /tmp/schema.dot > /srv/wsgi/static/schema.png')
    os.remove('/tmp/schema.dot')


@manage.command()
def test():
    os.system('python setup.py test')


@manage.command()
def run():
    wsgi.app.run(host='', debug=True, threaded=True)


@manage.command()
def worker():
    os.system('celery worker --config wsgi.celeryconfig -A wsgi.tasks'
        ' --logfile=celery.log')


if __name__ == '__main__':
    manage()
