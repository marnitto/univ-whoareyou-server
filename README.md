# whoareyou

아두이노와 라즈베리 파이를 활용한 방범 시스템 - 서버

## Install & Start

```
git clone git@bitbucket.org:jhgod/univ-whoareyou-server.git
cd univ-whoareyou-server
docker-compose build
docker-compose up -d
```

MariaDB 계정 정보는 /var/lib/mysql에 별도로 저장됩니다. 데이터를 완전히
삭제하고 싶다면 root 권한으로 삭제해야 합니다.

## Uninstall

`docker-compose rm`

## Web restart

```
docker-compose build
docker-compose stop
docker-compose rm -f
docker-compose up -d
```

or

```
python dev.py rebuild
```

## Unittest in Docker container

```
docker exec -i -t univwhoareyouserver_web_1 python manage.py test
```

or

```
python dev.py test
```
