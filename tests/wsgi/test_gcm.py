# -*- coding: utf-8 -*-

from __future__ import absolute_import

import json

import pytest
import requests
import responses

from wsgi import gcm


@responses.activate
def test_gcm_request_to_google():
    body = {
        'multicast_id': 1234,
        'success': 1,
        'failure': 0,
        'canonical_ids': 0,
        'results': [
            {'message_id': '0:abcd'}
        ]
    }
    google_gcm_server = 'https://gcm-http.googleapis.com/gcm/send'

    expect_token = 'client1'
    expect_title = 'title'
    expect_message = 'message'
    responses.add(responses.POST, google_gcm_server,
        status=200,
        body=json.dumps(body),
        content_type='application/json; charset=UTF-8')

    resp = gcm.send(
        req_id=expect_token, title=expect_title, message=expect_message)
    assert json.loads(resp) == body

    assert len(responses.calls) == 1

    req = responses.calls[0].request
    assert req.url == google_gcm_server
    assert req.headers['Authorization'].startswith('key=')
    assert req.headers['Content-Type'] == 'application/json'

    body = json.loads(req.body)
    assert body['data']['title'] == expect_title
    assert body['data']['message'] == expect_message
    assert body['to'] == expect_token
