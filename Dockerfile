FROM ubuntu:latest
MAINTAINER marnitto@gmail.com

# Install default ubuntu web server environment
RUN \
    apt-get update && apt-get install -y --no-install-recommends \
        python-software-properties \
        python-setuptools \
        build-essential \
        supervisor \
        nginx \
        gunicorn \
        python-dev \
        python \
        mysql-client \
        libmysqlclient-dev \
        graphviz \
        git \
        libmagickwand-dev
RUN /etc/init.d/nginx stop

# Install python server environment
RUN easy_install pip
RUN pip install --upgrade setuptools
RUN mkdir -p /var/log/supervisor

# Nginx configure
ADD ./nginx-app.conf /srv/
RUN rm /etc/nginx/sites-available/default
RUN rm /etc/nginx/sites-enabled/default
RUN ln -s /srv/nginx-app.conf /etc/nginx/sites-enabled/default
RUN mkdir -p /etc/nginx/ssl/

# Supervisord configure
ADD ./supervisord.conf /srv/
RUN ln -s /srv/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Run as normal user
RUN groupadd sw && useradd --home-dir /srv -g sw -G adm sw
RUN chmod 0755 /srv
RUN chown -R sw:sw /srv

# Install our code
ADD . /srv/
RUN chown -R sw:sw /srv/

# Install webapp envioronment
WORKDIR /srv
RUN pip install -e .

# Serve
EXPOSE 80
CMD ["/usr/bin/supervisord"]
