# -*- coding: utf-8 -*-

import os
import sys
import subprocess
from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass to py.test")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = ['-v']

    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        # Remove all *.pyc file
        os.system("""find . -name "*.pyc" -exec rm -rf {} \;""")

        # import here, cause outside the eggs aren't loaded
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)


install_requires = {
    'SQLAlchemy >= 1.0.8',  # Database ORM
    'alembic >= 0.8.2',  # SQLALchemy DB migration
    'MySQL-python >= 1.2.5',  # MySQL DB connector
    'sadisplay >= 0.4.0',  # DB schema drawing
    'flask >= 0.10',  # webapp
    'celery >= 3.1.18',  # Background processing
    'click >= 5.1',  # Manager CLI
    'requests >= 2.8.1',  # http(s) request
    'Wand >= 0.4.1'  # Image processing
}


tests_require = {
    'pytest >= 2.8.2',
    'pytest-cov >= 2.2.0',
    'pyflakes >= 1.0.0',
    'python-magic >= 0.4.6',
    'pexpect >= 4.0.1',
    'setuptools >= 17.1',  # `mock` dependency
    'responses >= 0.4.0'  # mocking `requests`
}


setup(
    name=u'whoareyou',
    description=u'아두이노와 라즈베리 파이를 활용한 방범 시스템 서버',
    author=u'Jihyeog Lim',
    packages=find_packages(exclude=['tests']),
    install_requires=install_requires,
    tests_require=tests_require,
    cmdclass={'test': PyTest},
    extras_require={
        'tests': tests_require
    }
)
